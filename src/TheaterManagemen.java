import java.util.ArrayList;


public class TheaterManagemen {
	Seat mon,tue,wed,thu,fri,sat,sun;
	public TheaterManagemen(){
		mon = new Seat("mon");
		tue = new Seat("tue");
		wed = new Seat("wed");
		thu = new Seat("thu");
		fri = new Seat("fri");
		sat = new Seat("sat");
		sun = new Seat("sun");
		mon.createshowtime1(DataSeatPrice.ticketprice);
		tue.createshowtime1(DataSeatPrice.ticketprice);
		wed.createshowtime1(DataSeatPrice.ticketprice);
		thu.createshowtime1(DataSeatPrice.ticketprice);
		fri.createshowtime1(DataSeatPrice.ticketprice);
		sat.createshowtime2(DataSeatPrice.ticketprice);
		sun.createshowtime2(DataSeatPrice.ticketprice);
	}
	public double buy(String m,int show,String rr,int c){
		int r = 0;
		double price;
		double[][] showtime = null;
		String[] s ={"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O"};
		for(int i=0;i<s.length;i++){
			if(rr.equals(s[i])){
				r= i;
			}
		}
		if(m.equals("mon")){
		if(show==1){
			showtime =mon.show1;
		}
		if(show==2){
			showtime =mon.show2;
		}
		}
		//---
		if(m.equals("tue")){
			if(show==1){
				showtime =tue.show1;
			}
			if(show==2){
				showtime =tue.show2;
			}
			}
		//--
		if(m.equals("wed")){
			if(show==1){
				showtime =wed.show1;
			}
			if(show==2){
				showtime =wed.show2;
			}
			}
		//--
		if(m.equals("thu")){
			if(show==1){
				showtime =thu.show1;
			}
			if(show==2){
				showtime =thu.show2;
			}
			}
		//--
		if(m.equals("fri")){
			if(show==1){
				showtime =fri.show1;
			}
			if(show==2){
				showtime =fri.show2;
			}
			}
		//--	
		if(m.equals("sat")){
		if(show==1){
			showtime =sat.show1;
		}
		if(show==2){
			showtime =sat.show2;
		}
		if(show==3){
			showtime =sat.show3;
		}
		}
	//--
	if(m.equals("sun")){
		if(show==1){
			showtime =sun.show1;
		}
		if(show==2){
			showtime =sun.show2;
		}
		if(show==3){
			showtime =sun.show3;
		}
		}
		price = showtime[r][c-1];
		if(price!=-1){
			if(m.equals("mon")){
				if(show==1){
					mon.set(mon.show1,r,c-1,0);
				}
				if(show==2){
					mon.set(mon.show2,r,c-1,0);
				}
				}
			if(m.equals("tue")){
				if(show==1){
					tue.set(tue.show1,r,c-1,0);
				}
				if(show==2){
					tue.set(tue.show2,r,c-1,0);
				}
				}
			if(m.equals("wed")){
				if(show==1){
					wed.set(wed.show1,r,c-1,0);
				}
				if(show==2){
					wed.set(wed.show2,r,c-1,0);
				}
				}
			if(m.equals("thu")){
				if(show==1){
					thu.set(thu.show1,r,c-1,0);
				}
				if(show==2){
					thu.set(thu.show2,r,c-1,0);
				}
				}
			if(m.equals("fri")){
				if(show==1){
					fri.set(fri.show1,r,c-1,0);
				}
				if(show==2){
					fri.set(fri.show2,r,c-1,0);
				}
				}
			if(m.equals("sat")){
				if(show==1){
					sat.set(sat.show1,r,c-1,0);
				}
				if(show==2){
					sat.set(sat.show2,r,c-1,0);
				}
				if(show==3){
					sat.set(sat.show3,r,c-1,0);
				}
				}
			if(m.equals("sun")){
				if(show==1){
					sun.set(sun.show1,r,c-1,0);
				}
				if(show==2){
					sun.set(sun.show2,r,c-1,0);
				}
				if(show==3){
					sun.set(sun.show3,r,c-1,0);
				}
				}
			
			return price;
		}
		else{
			return 0;
		}
		
	}
	
	public ArrayList<String> buy (double val){
		String[] s ={"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O"};
		ArrayList<String> ss = new ArrayList<String>();
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(mon.show1[i][j]==val){
					ss.add(mon.day+"1 : "+s[i]+(j+1));
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(mon.show2[i][j]==val){
					ss.add(mon.day+"2 : "+s[i]+(j+1));
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(tue.show1[i][j]==val){
					ss.add(tue.day+"1 : "+s[i]+(j+1));
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(tue.show2[i][j]==val){
					ss.add(tue.day+"2 : "+s[i]+(j+1));
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(wed.show1[i][j]==val){
					ss.add(wed.day+"1 : "+s[i]+(j+1));
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(wed.show2[i][j]==val){
					ss.add(wed.day+"2 : "+s[i]+(j+1));
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(thu.show1[i][j]==val){
					ss.add(thu.day+"1 : "+s[i]+(j+1));
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(thu.show2[i][j]==val){
					ss.add(thu.day+"2 : "+s[i]+(j+1));
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(fri.show1[i][j]==val){
					ss.add(fri.day+"1 : "+s[i]+(j+1));
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(fri.show2[i][j]==val){
					ss.add(fri.day+"2 : "+s[i]+(j+1));
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(sat.show1[i][j]==val){
					ss.add(sat.day+"1 : "+s[i]+(j+1));
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(sat.show2[i][j]==val){
					ss.add(sat.day+"2 : "+s[i]+(j+1));
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(sat.show3[i][j]==val){
					ss.add(sat.day+"3 : "+s[i]+(j+1));
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(sun.show1[i][j]==val){
					ss.add(sun.day+"1 : "+s[i]+(j+1));
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(sun.show2[i][j]==val){
					ss.add(sun.day+"2 : "+s[i]+(j+1));
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(sun.show3[i][j]==val){
					ss.add(sun.day+"3 : "+s[i]+(j+1));
				}
			}
		}
		
		
		return ss;
	}
	public ArrayList<String> buy(String rr,int c){
		String[] s ={"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O"};
		ArrayList<String> ss = new ArrayList<String>();
		int r = 0;
		double price=0;
		for(int i=0;i<s.length;i++){
			if(rr.equals(s[i])){
				r= i;
				ss.add(s[i]+c+" = ");
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(i==r && j==c){
					price = mon.show1[r][c-1];
					ss.add(mon.day+"1 : "+price);
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(i==r && j==c){
					price = mon.show2[r][c-1];
					ss.add(mon.day+"2 : "+price);
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(i==r && j==c){
					price = tue.show1[r][c-1];
					ss.add(tue.day+"1 : "+price);
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(i==r && j==c){
					price = tue.show2[r][c-1];
					ss.add(tue.day+"2 : "+price);
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(i==r && j==c){
					price = wed.show1[r][c-1];
					ss.add(wed.day+"1 : "+price);
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(i==r && j==c){
					price = wed.show2[r][c-1];
					ss.add(wed.day+"2 : "+price);
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(i==r && j==c){
					price = thu.show1[r][c-1];
					ss.add(thu.day+"1 : "+price);
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(i==r && j==c){
					price = thu.show2[r][c-1];
					ss.add(thu.day+"2 : "+price);
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(i==r && j==c){
					price = fri.show1[r][c-1];
					ss.add(fri.day+"1 : "+price);
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(i==r && j==c){
					price = fri.show2[r][c-1];
					ss.add(fri.day+"2 : "+price);
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(i==r && j==c){
					price = sat.show1[r][c-1];
					ss.add(sat.day+"1 : "+price);
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(i==r && j==c){
					price = sat.show2[r][c-1];
					ss.add(sat.day+"2 : "+price);
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(i==r && j==c){
					price = sat.show3[r][c-1];
					ss.add(sat.day+"3 : "+price);
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(i==r && j==c){
					price = sun.show1[r][c-1];
					ss.add(sun.day+"1 : "+price);
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(i==r && j==c){
					price = sun.show2[r][c-1];
					ss.add(sun.day+"2 : "+price);
				}
			}
		}
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				if(i==r && j==c){
					price = sun.show3[r][c-1];
					ss.add(sun.day+"3 : "+price);
				}
			}
		}
		return ss;
	}
}
